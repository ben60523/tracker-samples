/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IotConnectionCertificate {

    @Expose
    @SerializedName("clientCertificate")
    private String clientCertificate;

    @Expose
    @SerializedName("privateKey")
    private String privateKey;

    @Expose
    @SerializedName("rootCa")
    private String rootCa;

    public IotConnectionCertificate(String clientCertificate, String privateKey, String rootCa) {
        this.clientCertificate = clientCertificate;
        this.privateKey = privateKey;
        this.rootCa = rootCa;
    }

    public String getClientCertificate() {
        return clientCertificate;
    }

    public void setClientCertificate(String clientCertificate) {
        this.clientCertificate = clientCertificate;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getRootCa() {
        return rootCa;
    }

    public void setRootCa(String rootCa) {
        this.rootCa = rootCa;
    }

    @Override
    public String toString() {
        return "iotConnectionCertificate{" +
                "clientCertificate='" + clientCertificate + '\'' +
                ", privateKey='" + privateKey + '\'' +
                ", rootCa='" + rootCa + '\'' +
                '}';
    }
}
