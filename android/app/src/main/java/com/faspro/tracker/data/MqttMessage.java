/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

public class MqttMessage {

    public String topic;
    public byte[] data;

    public MqttMessage(String topic, byte[] data) {
        this.topic = topic;
        this.data = data;
    }

    public String getTopic() {
        return topic;
    }

    public byte[] getData() {
        return data;
    }

    public String getDataString() {
        return new String(data);
    }

}
