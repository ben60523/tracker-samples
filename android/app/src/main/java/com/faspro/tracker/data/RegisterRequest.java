/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterRequest {

    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("regToken")
    private String regToken;
    @Expose
    @SerializedName("phone")
    private String phone;
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("password")
    private String password;

    public RegisterRequest(String email, String name, String regToken, String phone, String type, String password) {
        this.email = email;
        this.name = name;
        this.regToken = regToken;
        this.phone = phone;
        this.type = type;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getRegToken() {
        return regToken;
    }

    public String getPhone() {
        return phone;
    }

    public String getType() {
        return type;
    }

    public String getPassword() {
        return password;
    }

}
