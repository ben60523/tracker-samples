/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.faspro.tracker.R;
import com.faspro.tracker.data.AddDeviceToGroup;
import com.faspro.tracker.data.AddDeviceToServerRequest;
import com.faspro.tracker.data.AddDeviceToServerResponse;
import com.faspro.tracker.data.CreateGroup;
import com.faspro.tracker.data.DeviceSettingResponse;
import com.faspro.tracker.data.GroupsResponse;
import com.faspro.tracker.data.IotConnection;
import com.faspro.tracker.data.IotConnectionCertificate;
import com.faspro.tracker.data.IotRequest;
import com.faspro.tracker.data.IotRequestDevice;
import com.faspro.tracker.data.IotResponse;
import com.faspro.tracker.data.LoginSession;
import com.faspro.tracker.data.MqttMessage;
import com.faspro.tracker.data.SavedDevice;
import com.faspro.tracker.data.SavedGroup;
import com.faspro.tracker.data.UpdateDeviceSettingRequest;
import com.faspro.tracker.data.UpdateDeviceSettingRequestDesired;
import com.faspro.tracker.data.UpdateSettingResponse;
import com.faspro.tracker.mqtt.ConnectionFactory;
import com.faspro.tracker.mqtt.ConnectionListener;
import com.faspro.tracker.mqtt.ConnectionManager;
import com.faspro.tracker.service.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseActivity implements View.OnClickListener, ConnectionListener, DeviceSettingFragment.OnDeviceSettingListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String GROUP_TITLE = "testGroup01";
    private static final String DEVICE_TYPE = "watch";
    private static final String MAC_ADDRESS = "84:FE:DC:52:76:A8";
    private static final int MSG_CONNECTING = 1;
    private static final int MSG_RECONNECTING = 2;
    private static final int MSG_CONNECTED = 3;
    private static final int MSG_DISCONNECTED = 4;
    private static final int MSG_RECEIVED = 5;
    private static final int MSG_SETTING_UPDATE = 6;
    private Button btnConnect, btnDisconnect, btnSubscribe, btnAddDevice, btnDeviceSetting, btnRequestLocation;
    private ConnectionManager connectionManager = null;
    private Toast mToast = null;
    private int mInterval = 0;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case MSG_CONNECTED:
                    showToast("Connected successfully!");
                    btnConnect.setEnabled(false);
                    btnDisconnect.setEnabled(true);
                    btnSubscribe.setEnabled(true);
                    btnRequestLocation.setEnabled(true);
                    break;
                case MSG_DISCONNECTED:
                    showToast("Disconnected!");
                    Log.d(TAG, "disconnected");
                    btnConnect.setEnabled(true);
                    btnDisconnect.setEnabled(false);
                    btnSubscribe.setEnabled(false);
                    btnRequestLocation.setEnabled(false);
                    break;
                case MSG_CONNECTING:
                    showToast("Connecting...");
                    btnConnect.setEnabled(false);
                    btnDisconnect.setEnabled(false);
                    btnSubscribe.setEnabled(false);
                    break;
                case MSG_RECONNECTING:
                    showToast("Reconnecting...");
                    btnConnect.setEnabled(false);
                    btnDisconnect.setEnabled(false);
                    btnSubscribe.setEnabled(false);
                    break;
                case MSG_RECEIVED:
                    MqttMessage mqttMessage = (MqttMessage) msg.obj;
                    showToast(mqttMessage.getTopic() + ": " + mqttMessage.getDataString());
                    break;
                case MSG_SETTING_UPDATE:
                    updateDeviceSettings();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnConnect = findViewById(R.id.btnConnect);
        btnDisconnect = findViewById(R.id.btnDisconnect);
        btnSubscribe = findViewById(R.id.btnSubscribe);
        btnAddDevice = findViewById(R.id.btnAddDevice);
        btnRequestLocation = findViewById(R.id.btnRequestLocation);
        btnDeviceSetting = findViewById(R.id.btnDeviceSetting);
        btnConnect.setOnClickListener(this);
        btnDisconnect.setOnClickListener(this);
        btnSubscribe.setOnClickListener(this);
        btnAddDevice.setOnClickListener(this);
        btnRequestLocation.setOnClickListener(this);
        btnDeviceSetting.setOnClickListener(this);

        SavedDevice savedDevice = getSavedDevice();
        if (savedDevice._id != null) {
            btnDeviceSetting.setEnabled(true);
        }

        findViewById(R.id.btnLogout).setOnClickListener(this);

    }

    private void getDeviceSettings() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        SavedDevice savedDevice = getSavedDevice();
        LoginSession loginSession = getSavedSession();
        Call<DeviceSettingResponse> call = api.getDeviceSettings(savedDevice.diviceId, loginSession.ugsToken);
        call.enqueue(new Callback<DeviceSettingResponse>() {
            @Override
            public void onResponse(Call<DeviceSettingResponse> call, Response<DeviceSettingResponse> response) {
                Log.d(TAG, "getDeviceSettings response: " + response.toString());
                if (200 == response.code()) {
                    showToast(response.toString());
                }
            }

            @Override
            public void onFailure(Call<DeviceSettingResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void updateDeviceSettings() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        LoginSession loginSession = getSavedSession();
        SavedDevice savedDevice = getSavedDevice();
        UpdateDeviceSettingRequestDesired updateDeviceSettingRequestDesired = new UpdateDeviceSettingRequestDesired(mInterval);
        UpdateDeviceSettingRequest updateDeviceSettingRequest = new UpdateDeviceSettingRequest(updateDeviceSettingRequestDesired);
        Call<UpdateSettingResponse> call = api.updateDeviceSettings(savedDevice.diviceId, loginSession.ugsToken, updateDeviceSettingRequest);
        call.enqueue(new Callback<UpdateSettingResponse>() {
            @Override
            public void onResponse(Call<UpdateSettingResponse> call, Response<UpdateSettingResponse> response) {
                Log.d(TAG, "updateDeviceSettings response: " + response.toString());
                if (200 == response.code()) {
                    Log.d(TAG, "updateDeviceSettings succeeded");
                }
            }

            @Override
            public void onFailure(Call<UpdateSettingResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void createGroup() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        LoginSession loginSession = getSavedSession();
        CreateGroup createGroup = new CreateGroup(GROUP_TITLE, "");
        Call<CreateGroup> call = api.createGroup(loginSession.ugsToken, createGroup);
        call.enqueue(new Callback<CreateGroup>() {
            @Override
            public void onResponse(Call<CreateGroup> call, Response<CreateGroup> response) {
                Log.d(TAG, "createGroup response: " + response.toString());
                showToast("createGroup result: " + response.code());
            }

            @Override
            public void onFailure(Call<CreateGroup> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void addGroupToUser() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        LoginSession loginSession = getSavedSession();
        SavedGroup savedGroup = getSavedGroup();
        SavedDevice savedDevice = getSavedDevice();
        AddDeviceToGroup addDeviceToGroup = new AddDeviceToGroup(savedDevice._id, savedGroup.groupId);
        Call<AddDeviceToGroup> call = api.addDeviceToGroup(savedGroup.groupId, loginSession.ugsToken, addDeviceToGroup);
        call.enqueue(new Callback<AddDeviceToGroup>() {
            @Override
            public void onResponse(Call<AddDeviceToGroup> call, Response<AddDeviceToGroup> response) {
                Log.d(TAG, "addDeviceToGroup response: " + response.toString());
                if (200 == response.code()) {
                    showToast("Device added into the group " + GROUP_TITLE);
                    btnDeviceSetting.setEnabled(true);
                } else {
                    showToast("addDeviceToGroup result: " + response.code() + ": " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AddDeviceToGroup> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void addDeviceToServer() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        LoginSession loginSession = getSavedSession();
        final AddDeviceToServerRequest device = new AddDeviceToServerRequest(MAC_ADDRESS, DEVICE_TYPE, "ben0", "0920674700", "", "2000-12-12", "male", 180, 70);
        Call<AddDeviceToServerResponse> call = api.addWearable(loginSession.ugsToken, device);
        call.enqueue(new Callback<AddDeviceToServerResponse>() {
            @Override
            public void onResponse(Call<AddDeviceToServerResponse> call, Response<AddDeviceToServerResponse> response) {
                Log.d(TAG, "addDeviceToServer response: " + response.toString());
                if (200 == response.code()) {
                    showToast("Device added: " + device.getName());
                    saveDeivce(response.body().getUser().getId(), device.getMAC(), response.body().getUser().getDeviceStatus().getDevice());
                    addGroupToUser();
                } else {
                    showToast("addDeviceToServer result: " + response.code() + ": " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AddDeviceToServerResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getGroups() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        LoginSession loginSession = getSavedSession();
        Call<GroupsResponse> call = api.getGroups("", "", loginSession.ugsToken);
        call.enqueue(new Callback<GroupsResponse>() {
            @Override
            public void onResponse(Call<GroupsResponse> call, Response<GroupsResponse> response) {
                String responses = String.valueOf(response.code());
                showToast("getGroups result: " + responses);
                Log.d(TAG, "getGroups response: " + response.toString());
                if (404 == response.code()) {
                    createGroup();
                }
                assert response.body() != null;
                saveGroup(response.body().getGroup().get(0).getId());
                addDeviceToServer();
            }

            @Override
            public void onFailure(Call<GroupsResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void connect() {
        showToast("Preparing for connection...");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);

        LoginSession loginSession = getSavedSession();

        IotRequestDevice device = new IotRequestDevice(loginSession.userId, DEVICE_TYPE);
        IotRequest iotRequest = new IotRequest(device);

        Call<IotResponse> call = api.connector(loginSession.ugsToken, iotRequest);
        call.enqueue(new Callback<IotResponse>() {
            @Override
            public void onResponse(Call<IotResponse> call, Response<IotResponse> response) {
                //Main UI Thread
                Log.d(TAG, "connect response: " + response.toString());
                if (200 == response.code()) {
                    showToast("Connecting...");

                    if (response.body() != null) {
                        final IotConnection iotConnection = response.body().getIotConnection();
                        IotConnectionCertificate cert = iotConnection.getCertificate();

                        String clientCertificate = cert.getClientCertificate();
                        String privateKey = cert.getPrivateKey();
                        String rootCa = cert.getRootCa();

                        SharedPreferences IotResponse = getSharedPreferences("IOT_RESPONSE", MODE_PRIVATE);
                        IotResponse.edit()
                                .putString("shared_clientCertificate", clientCertificate)
                                .putString("shared_privateKey", privateKey)
                                .putString("shared_rootCa", rootCa)
                                .apply();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                connectionManager = ConnectionFactory.initConnectionManager(MainActivity.this, iotConnection);
                            }
                        }).start();
                    }
                } else {
                    showToast("Failed to connect!");
                }
            }

            @Override
            public void onFailure(Call<IotResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void disconnect() {
        if (connectionManager != null) {
            connectionManager.disconnect();
        }
    }

    public void subscribe() {
        if (connectionManager != null) {
            SavedGroup savedGroup = getSavedGroup();
            String topic = "fsp/group/" + savedGroup.groupId + "/#";
            connectionManager.subscribe(topic);
        }
    }

    public void requestLocation() {
        if (connectionManager != null) {
            SavedDevice savedDevice = getSavedDevice();
            String topic = "fsp/user/" + savedDevice._id + "/loc/request_location";
            String message = "request_location";
            connectionManager.publish(message, topic);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnConnect:
                connect();
                break;
            case R.id.btnDisconnect:
                disconnect();
                break;
            case R.id.btnSubscribe:
                subscribe();
                break;
            case R.id.btnLogout:
                clearSavedSession();
                finish();
                break;
            case R.id.btnAddDevice:
                getGroups();
                break;
            case R.id.btnDeviceSetting:
                getDeviceSettings();
                showSettingDialog(view);

                break;
            case R.id.btnRequestLocation:
                requestLocation();
                break;

        }
    }

    private void showToast(String text) {
        if (mToast != null) mToast.cancel();
        mToast = Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT);
        mToast.show();
    }

    public void showSettingDialog(View view) {
        DeviceSettingFragment deviceSettingFragment = new DeviceSettingFragment();
        deviceSettingFragment.setOnDeviceSettingListener(this);
        deviceSettingFragment.show(getSupportFragmentManager(), "getDeviceSettings");
    }

    @Override
    public void onConnected() {
        mHandler.obtainMessage(MSG_CONNECTED).sendToTarget();
    }

    @Override
    public void onDisconnected(Throwable throwable) {
        mHandler.obtainMessage(MSG_DISCONNECTED).sendToTarget();
    }

    @Override
    public void onConnecting(Throwable throwable) {
        mHandler.obtainMessage(MSG_CONNECTING).sendToTarget();
    }

    @Override
    public void onReconnecting(Throwable throwable) {
        mHandler.obtainMessage(MSG_RECONNECTING).sendToTarget();
    }

    @Override
    public void onMessageArrived(String topic, byte[] data) {
        Message msg = mHandler.obtainMessage(MSG_RECEIVED);
        msg.obj = new MqttMessage(topic, data);
        msg.sendToTarget();
    }

    @Override
    public void onSettingComplete(int interval) {
        showToast(String.valueOf(interval));
        Log.d(TAG, "onSettingComplete: interval=" + interval);
        mHandler.obtainMessage(MSG_SETTING_UPDATE).sendToTarget();
        mInterval = interval;
    }
}
