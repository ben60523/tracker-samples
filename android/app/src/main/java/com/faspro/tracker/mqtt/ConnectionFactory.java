/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.mqtt;

import android.app.Activity;

import com.faspro.tracker.data.IotConnection;

public class ConnectionFactory {

    private static ConnectionManager manager = null;

    public static ConnectionManager getConnectionManager(Activity activity) {
        if (manager != null && activity instanceof ConnectionListener) {
            manager.addListener((ConnectionListener) activity);
        }
        return manager;
    }

    public static ConnectionManager initConnectionManager(Activity activity, IotConnection iotConnection) {
        if (iotConnection != null) {
            if (manager != null) {
                manager.close();
            }
            manager = new ConnectionManager(activity, iotConnection);
        }
        return getConnectionManager(activity);
    }
}
