/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.service;

import com.faspro.tracker.data.AddDeviceToGroup;
import com.faspro.tracker.data.AddDeviceToServerRequest;
import com.faspro.tracker.data.AddDeviceToServerResponse;
import com.faspro.tracker.data.CreateGroup;
import com.faspro.tracker.data.DeviceSettingResponse;
import com.faspro.tracker.data.GroupsResponse;
import com.faspro.tracker.data.IotRequest;
import com.faspro.tracker.data.IotResponse;
import com.faspro.tracker.data.LoginRequest;
import com.faspro.tracker.data.LoginResponse;
import com.faspro.tracker.data.RegToken;
import com.faspro.tracker.data.RegisterRequest;
import com.faspro.tracker.data.UpdateDeviceSettingRequest;
import com.faspro.tracker.data.UpdateSettingResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @POST("/ugs/api/user/regtoken")
    Call<RegToken> regToken(@Body RegToken regToken);

    @POST("/ugs/api/user/register")
    Call<RegisterRequest> register(@Body RegisterRequest registerRequest);

    @POST("ugs/api/user/login")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @POST("/ugs/api/iot/connector")
    Call<IotResponse> connector(
            @Query("ugs_token") String ugsToken,
            @Body IotRequest iotRequest
    );

    @POST("/ugs/api/group")
    Call<CreateGroup> createGroup(
            @Query("ugs_token") String ugsToken,
            @Body CreateGroup createGroup
    );

    @GET("/ugs/api/group")
    Call<GroupsResponse> getGroups(
            @Query("where") String where,
            @Query("limit") String limit,
            @Query("ugs_token") String ugsToken
    );

    @POST("/ugs/api/user/addwearable")
    Call<AddDeviceToServerResponse> addWearable(
            @Query("ugs_token") String ugsToken,
            @Body AddDeviceToServerRequest addDeviceToServerRequest
    );

    @POST("/ugs/api/group/{groupId}/user")
    Call<AddDeviceToGroup> addDeviceToGroup(
            @Path("groupId") String groupId,
            @Query("ugs_token") String ugsToken,
            @Body AddDeviceToGroup addDeviceToGroup
    );

    @GET("/accounts/api/devices/{deviceId}/settings")
    Call<DeviceSettingResponse> getDeviceSettings(
            @Path("deviceId") String deviceId,
            @Query("ugs_token") String ugsToken
    );

    @PUT("/accounts/api/devices/{deviceId}/settings")
    Call<UpdateSettingResponse> updateDeviceSettings(
            @Path("deviceId") String deviceId,
            @Query("ugs_token") String ugsToken,
            @Body UpdateDeviceSettingRequest updateDeviceSettingRequest
    );
}
