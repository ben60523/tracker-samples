/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

public class SavedDevice {
    public String _id;
    public String deviceMAC;
    public String diviceId;

    public SavedDevice(String _id, String deviceMAC, String deviceId) {
        this._id = _id;
        this.deviceMAC = deviceMAC;
        this.diviceId = deviceId;
    }
}
