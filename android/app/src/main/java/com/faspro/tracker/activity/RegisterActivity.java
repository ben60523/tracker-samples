/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.faspro.tracker.R;
import com.faspro.tracker.data.RegToken;
import com.faspro.tracker.data.RegisterRequest;
import com.faspro.tracker.service.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private EditText etEmail, etPassword, etName, etPhone, etEnterCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etEnterCode = findViewById(R.id.etEnterCode);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etName = findViewById(R.id.etName);
        etPhone = findViewById(R.id.etPhone);

        findViewById(R.id.bRegister).setOnClickListener(this);
        findViewById(R.id.bVerifyEmail).setOnClickListener(this);
        findViewById(R.id.bResendCode).setOnClickListener(this);
        findViewById(R.id.tvLogin).setOnClickListener(this);
    }

    private void verifyEmail() {
        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        RegToken regToken = new RegToken(etEmail.getText().toString());

        Call<RegToken> call = api.regToken(regToken);
        call.enqueue(new Callback<RegToken>() {
            @Override
            public void onResponse(Call<RegToken> call, Response<RegToken> response) {
                if (200 == response.code()) {
                    Toast.makeText(RegisterActivity.this, "Token is generated and sent to your email", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(RegisterActivity.this, "Verify Failure! Please Verify again!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegToken> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void resendCode() {

    }

    private void userSignUp() {
        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        RegisterRequest registerRequest = new RegisterRequest(etEmail.getText().toString(), etName.getText().toString(), etEnterCode.getText().toString(), etPhone.getText().toString(), "supervisor", etPassword.getText().toString());

        Call<RegisterRequest> call = api.register(registerRequest);
        call.enqueue(new Callback<RegisterRequest>() {
            @Override
            public void onResponse(Call<RegisterRequest> call, Response<RegisterRequest> response) {
                if (200 == response.code()) {
                    Toast.makeText(RegisterActivity.this, "Register success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(RegisterActivity.this, "Register Failure! Please Register again!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterRequest> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bVerifyEmail:
                verifyEmail();
                break;
            case R.id.bResendCode:
                resendCode();
                break;
            case R.id.bRegister:
                userSignUp();
                break;
            case R.id.tvLogin:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }
}
