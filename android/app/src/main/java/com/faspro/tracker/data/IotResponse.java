/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IotResponse {
    @Expose
    @SerializedName("code")
    private int code;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("iotConnection")
    private IotConnection iotConnection;

    public IotResponse(int code, String message, IotConnection iotConnection) {
        this.code = code;
        this.message = message;
        this.iotConnection = iotConnection;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IotConnection getIotConnection() {
        return iotConnection;
    }

    public void setIotConnection(IotConnection iotConnection) {
        this.iotConnection = iotConnection;
    }

    @Override
    public String toString() {
        return "IotResponse{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", iotConnection=" + iotConnection +
                '}';
    }
}
