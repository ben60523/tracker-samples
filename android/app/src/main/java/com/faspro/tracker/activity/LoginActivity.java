/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.faspro.tracker.R;
import com.faspro.tracker.data.LoginRequest;
import com.faspro.tracker.data.LoginResponse;
import com.faspro.tracker.service.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String LOGIN_TYPE = "supervisor";
    private EditText etEmail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);

        findViewById(R.id.bLogin).setOnClickListener(this);
        findViewById(R.id.tvRegister).setOnClickListener(this);

        checkLoginSession();
    }

    private void userLogin() {
        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);

        LoginRequest loginRequest = new LoginRequest(etEmail.getText().toString(), LOGIN_TYPE, etPassword.getText().toString());

        Call<LoginResponse> call = api.login(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (200 == response.code()) {

                    Toast.makeText(LoginActivity.this, "Logged-in successfully!", Toast.LENGTH_SHORT).show();

                    if (response.body() != null) {
                        String ugsToken = response.body().getUgsToken();
                        String userId = response.body().getUser().getId();
                        saveLoginSession(ugsToken, userId);
                    }

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(LoginActivity.this, "Login Failed!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                t.printStackTrace();
            }

        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bLogin:
                userLogin();
                break;
            case R.id.tvRegister:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

    private void checkLoginSession() {
        if (getSavedSession().isValid()) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }
    }
}
