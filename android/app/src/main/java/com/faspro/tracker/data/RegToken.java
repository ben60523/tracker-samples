/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegToken {
    @Expose
    @SerializedName("email")
    private String email;

    public RegToken(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
