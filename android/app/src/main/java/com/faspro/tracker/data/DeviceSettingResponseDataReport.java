/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceSettingResponseDataReport {

    @Expose
    @SerializedName("locPubInt")
    private int interval;

    @Expose
    @SerializedName("locPubStart")
    private String locPubStart;

    @Expose
    @SerializedName("locPubEnd")
    private String locPubEnd;

    public String getLocPubStart() {
        return locPubStart;
    }

    public String getLocPubEnd() {
        return locPubEnd;
    }

    public int getInterval() {
        return interval;
    }

}
