/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

public class LoginSession {

    public String ugsToken;
    public String userId;

    public LoginSession(String ugsToken, String userId) {
        this.ugsToken = ugsToken;
        this.userId = userId;
    }

    public boolean isValid() {
        return ugsToken != null && userId != null;
    }
}
