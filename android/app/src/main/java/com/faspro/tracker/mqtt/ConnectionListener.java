/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.mqtt;

public interface ConnectionListener {
    void onConnecting(Throwable throwable);

    void onReconnecting(Throwable throwable);

    void onConnected();

    void onDisconnected(Throwable throwable);

    void onMessageArrived(String topic, byte[] data);
}