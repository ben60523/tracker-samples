/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddDeviceToServerResponseUser {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("mac")
    @Expose
    private String mac;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("deviceStatus")
    @Expose
    private AddDeviceToServerResponseUserDeviceStatus deviceStatus;

    public AddDeviceToServerResponseUserDeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
