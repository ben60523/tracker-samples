/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateGroup {
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("photo_base64")
    private String photoBase64;

    public CreateGroup(String title, String photoBase64) {
        this.photoBase64 = photoBase64;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getPhotoBase64() {
        return photoBase64;
    }

}
