'use-strict'
const request = require('superagent')
const hostUrl = "https://fsp.borqs.io/"

let actions = {
    /**
     * Sign up here https://dm.fsp.borqs.io/#/signin if you don't have account yet.
     * 
     * Sign in with e-mail and password
     * @param {Object} usrInfo - {email: "your e-mail", password: "your password", type: "supervisor"}
     * 
     */
    signin: function (usrInfo) {
        return new Promise((resolve, reject) => {
            request.post(`${hostUrl}accounts/api/users/login`).send(usrInfo).then((res) => {
                if (res.ok) {
                    if (res.body) {
                        resolve(res.body)
                    }
                } else {
                    reject(res)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },
    /**
     * Get group information in the user account
     * @param {String} ugsToken - the token of the user account in the UGS server, retrieved by signin 
     * 
     */
    getGroupInfo: function (ugsToken) {
        return new Promise((resolve, reject) => {
            request.get(`${hostUrl}ugs/api/group?ugs_token=${ugsToken}`).then((res) => {
                if (res.ok) {
                    if (res.body) {
                        resolve(res.body)
                    }
                } else {
                    reject(res)
                }
            }).catch((err => {
                reject(err)
            }))
        })
    },
    /**
     * get detail information of user
     * 
     * @param {String} userid  - the ID of user in the UGS server, retrieved by signin  
     * @param {String} ugsToken - the token of the user account in the UGS server, retrieved by signin   
     */
    getUserDetails: function (userid, ugsToken) {
        return new Promise((resolve, reject) => {
            request.get(`${hostUrl}ugs/api/user/${userid}?ugs_token=${ugsToken}&isPopulateGroup=true&isPopulateUserDevices=false`).then((res) => {
                if (res.ok) {
                    resolve(res.body.user)
                } else {
                    reject(res.body)
                }
            }).catch((err) => {
                reject(err)
            })
        })
    },
    /**
     * get clientCert, privateKey, caCert, host and clientId for MQTT connection
     * @param {String} userid  - the ID of user in the UGS server, retrieved by signin 
     * @param {String} ugsToken - the token of the user account in the UGS server, retrieved by signin  
     */
    iotConnector: function (userid, ugsToken) {
        return new Promise((resolve, reject) => {
            request
                .post(`${hostUrl}ugs/api/iot/connector?ugs_token=${ugsToken}`)
                .send(
                    {
                        device:
                        {
                            id: userid,
                            type: 'watch'
                        }
                    })
                .then((res) => {
                    if (res.ok) {
                        resolve(res.body.iotConnection)
                    } else {
                        reject(res.body)
                    }
                })
                .catch((err) => {
                    reject(err)
                })
        })
    },
    /**
     * Update tracker setting
     * @param {String} deviceID - the id of the tracker, retrieved by getUserDetails
     * @param {String} ugsToken - the token of the user account in the UGS server, it will be retrieved by signin 
     * @param {Object} desiredObj - the setting you want to change
     * *****************************The format of desiredObj************************************************
     * {
     *  locPubInt: 300,
     *  locPubStart: "7:00",
     *  locPubEnd: "23:00",
     *  locPubDays: "1.1.1.1.1.1.1",
     *  safezones: [
     *      {
     *          lat: 12,
     *          lng: 77,
     *          radius: 100,
     *          title: "office safezone",
     *          address: "Queens Road, Bangalore",\
     *          type: "Work"
     *      }
     *  ]
     *  phonebooks: [
     *      {
     *          name: "SOS Number",
     *          type: "sos",
     *          priority: 1,
     *          number: "9090909090"
     *      }
     *  ]
     * }
     * *****************************The format of desiredObj************************************************
     */
    updateSetting: function (deviceID, ugsToken, desiredObj) {
        return new Promise((resolve, reject) => {
            request.put(`${hostUrl}accounts/api/devices/${deviceID}/settings?ugs_token=${ugsToken}`)
                .send({
                    desired: desiredObj
                }).then((res) => {
                    if (res.ok) {
                        resolve(res.body)
                    } else {
                        reject(res.body)
                    }
                }).catch((err) => {
                    reject(err)
                })
        })
    },
    /**
     * Get tracker setting
     * @param {String} deviceID - the id of the tracker, retrieved by getUserDetails
     * @param {String} ugsToken - the token of the user account in the UGS server, retrieved by signin 
     * 
     */
    getSetting: function (deviceID, ugsToken) {
        return new Promise((resolve, reject) => {
            request.get(`${hostUrl}accounts/api/devices/${deviceID}/settings?ugs_token=${ugsToken}&phonebook=true`).then((res) => {
                if (res.ok) {
                    resolve(res.body.data.desired)
                } else {
                    reject(res.body)
                }
            }).catch((err) => {
                reject(err)
            })
        })
    }
}

module.exports = actions
