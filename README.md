# tracker-samples

Sample codes for FASPRO Trackers, including:

* android - Android client which demonstrates:
    * User login
    * Register devices
    * Subscribe to topics
    * Change device settings
* collections - RESTful API collections for Postman
* php - Test scripts in PHP
* script - Test scripts in BASH

Ref: https://hackmd.io/@watson/HkreVO-tH